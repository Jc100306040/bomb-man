// 主要介面

import React, { Component } from 'react';
import { layer_type, game_type } from './const/const.js';
import Home from './Home.js';
import Lobby from './Lobby.js';
import BtnList from './BtnList.js';

class Main extends Component 
{


    constructor(props)
    {
        super(props);

        // 設置 Call Back Refs 
        this.node_layer = null;
        this.set_node_layer = ele => { this.node_layer = ele };

        this.state = 
            {
                // 層種類，預設為首頁
                layer_type : layer_type.HOME,

                // 遊戲種類，預設為null
                game_layer : null,

                // 是否正在刷新遊戲
                isRefreshGame : false,
            }; 
    }


    // 畫出按鈕清單
    drawButtonList()
    {
        return <BtnList layerType={this.state.layer_type} gameLayer={this.state.gameLayer}
        func_enterLobby={this.enterLobby.bind(this)} func_refreshGame={this.refreshGame.bind(this)} gameLayer={this.state.game_layer}/>;
    }  


    // 進入大廳
    enterLobby = () =>
    {
        this.setState({
            layer_type : layer_type.LOBBY,
            game_layer : null
        });    
    }


    // 進入遊戲
    enterGame = (gameLayer) =>
    {
        this.setState({
            layer_type : layer_type.GAME,
            game_layer : gameLayer
        });
    }


    // 重置遊戲
    refreshGame = () =>
    {
        this.setState({ isRefreshGame : true});
        setTimeout(()=>{
            this.setState({ isRefreshGame : false });
        }, 10); 
    }


    render()
    {
        let height = window.innerHeight;

        // 主頁
        let home_layer = (this.state.layer_type == layer_type.HOME)? <Home cbFunc={this.enterLobby} /> : null;

        // 大廳
        let lobby_layer = (this.state.layer_type == layer_type.LOBBY)? <Lobby cbFunc={this.enterGame.bind(this)} /> : null;

        // 遊戲
        let game_layer = (this.state.layer_type == layer_type.GAME && this.state.game_layer && !this.state.isRefreshGame)? this.state.game_layer : null;
        
        return (
            <div className="" ref={this.set_node_layer} style={{height:"100%", width:"100%", position:"relative"}}>
                {home_layer}
                {lobby_layer}
                {game_layer}
                {this.drawButtonList()}
            </div>
        );
    }


}

export default Main;

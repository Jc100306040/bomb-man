// 按鈕元件

import React, { Component } from 'react';

class Btn extends Component
{


    constructor(props)
    {
        super(props);
    }


    onClickFunction = () => 
    {
        this.props.cbFunc && this.props.cbFunc();
    }


    render()
    {
        let text = this.props.showText? this.props.text : ""; 
        let url = this.props.backgroundUrl;
        let backgroundImage = "url(" + url + ")";
        
        return (
          <div className="btn float-left" onClick={this.onClickFunction}>
              <div className="btn-icon" style={{backgroundImage:backgroundImage}} />
          </div>
        );
    }


}

export default Btn;

// 按鈕列表


import React, { Component } from 'react';
import { btn_img, btn_type, layer_type, game_type } from './const/const.js';
import Btn from './Btn.js';


class BtnList extends Component
{


    constructor(props)
    {
        super(props);
    }


    drawButtons()
    {
        let btns = [];
        for(let key in btn_type)
        {
            let type = btn_type[key];
            let cbFunc = null;
            
            switch(type)
            { 
                //  返回上頁
                case btn_type.BACK :
                    if(this.props.layerType == layer_type.HOME || this.props.layerType == layer_type.LOBBY) continue;
                    if(this.props.func_enterLobby) 
                    {
                        cbFunc = this.props.func_enterLobby;
                    }
                    break;
                
                //  重新遊戲
                case btn_type.REPLAY :
                    if(this.props.layerType == layer_type.HOME || this.props.layerType == layer_type.LOBBY) continue;
                    if(this.props.func_refreshGame) 
                    {
                        cbFunc = this.props.func_refreshGame;
                    }
			              break;
            }

            let img_url = btn_img[type];
            btns.push(< Btn backgroundUrl={img_url} cbFunc={cbFunc}/>);
        }
        return btns;
    }


    render()
    {
        return (
          <div className="btn-list">
            {this.drawButtons()}
          </div>
        );
    }


}

export default BtnList;

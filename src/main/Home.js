// 首頁


import React, { Component } from 'react';
import { home_text } from './const/const.js';
import UTILS from '../utils/Utils.js'


class Home extends Component 
{

    constructor(props)
    {
        super(props);

        // 是否已經點擊
        this.isClick = false;

        // 設置 Call Back Refs 
        this.node_bg = null;
        this.set_node_bg = ele => { this.node_bg = ele };

        this.node_title = null;
        this.set_node_title = ele => { this.node_title = ele };
    }


    // 點擊文字
    tap_txt = () =>
    {
        if(this.isClick) return;
        this.isClick = true;

        this.node_title.style.color = "black";
        this.node_title.style.top = "-10%";
        this.node_bg.style.backgroundColor = "white";

        setTimeout( () => { this.props.cbFunc && this.props.cbFunc() }, 1100);
    }


    render()
    {
        let text = home_text;
        let style_title = UTILS.getTransitionStyle(["color", "0.7s"], ["top", "0.4s", "0.4s"]);
        let style_bg    = UTILS.getTransitionStyle(["backGround", "1.2s"]);
  
        return (
          <div className="black fix-center-background" style={style_bg} ref={this.set_node_bg}>
            <div className="home-title-text" style={style_title} ref={this.set_node_title} onClick={this.tap_txt}>
              {text}
            </div>
          </div>
        );
    }

}

export default Home;

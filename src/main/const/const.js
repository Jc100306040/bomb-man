import React, { Component } from 'react';
import BoardMain from '../../bomb-man/BoardMain.js';
import Maze from '../../maze/Maze.js';
import CubeBlast from '../../cube-blast/CubeBlast.js';


// 定義層種類
const layer_type = 
  	{
      	HOME   : "HOME",
      	LOBBY  : "LOBBY",
      	GAME   : "GAME"
  	};

// 定義遊戲種類
const game_type = 
	{	
  		BOMB_MAN  	: "BOMB_MAN",
  		MAZE      	: "MAZE",
			CUBE_BLAST	: "CUBE_BLAST"
	}

// 定義遊戲文字
const game_text = 
  {
      [game_type.BOMB_MAN]  	: "BOMB MAN",
      [game_type.MAZE]      	: "MAZE",
			[game_type.CUBE_BLAST]	: "CUBE BLAST"
  }

// 定義遊戲層
const game_layer = 
  {
      [game_type.BOMB_MAN]  	: <BoardMain />,
      [game_type.MAZE]      	: <Maze />,
			[game_type.CUBE_BLAST]  : <CubeBlast />
  }

// 定義功能按鈕種類
const btn_type = 
	{
			BACK		: "BACK",
			REPLAY	:	"REPLAY",
			// HOME    : "HOME"
	} 

// 定義功能按鈕圖片資源路徑
const btn_img = 
	{
			[btn_type.BACK]		: require("../../pic/back.png"),
			[btn_type.REPLAY]	: require("../../pic/replay.png"),
			[btn_type.HOME]		: require("../../pic/replay.png")
	}

// 主頁文字
const home_text = "JIM'S WORK";


export { layer_type, game_type, game_text, game_layer, home_text, btn_type, btn_img};
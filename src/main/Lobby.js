// 遊戲大廳


import React, { Component } from 'react';
import { game_type, game_text, game_layer } from './const/const.js';


class Lobby extends Component 
{


  constructor(props)
  {
      super(props);

      // 存取 cb Function
      this.cbFunc = this.props.cbFunc;
  }


  // 繪製遊戲元件
  drawGameItems()
  {
      let gameItemArr = [];
      for(let key in game_type)
      {
          let gameType = game_type[key];
          let txt = game_text[gameType];
          let gameLayer = game_layer[gameType];
          let onClickFunc = () => { this.cbFunc(gameLayer) };
          
          let item = (<div className="lobby-item float-left" onClick={onClickFunc}>{txt}</div>);  
          gameItemArr.push(item);
      }
      return gameItemArr;
  }


  render()
  {
      return (
        <div style={{position:"absolute", width:"100%", height:"100%"}}>
            <div className="lobby-item-container">
                {this.drawGameItems()}
            </div>
        </div>
      );
  }


}

export default Lobby;

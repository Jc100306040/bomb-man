// 格子元件

import React, { Component } from 'react';

class Grid extends Component 
{

    constructor(props)
    {
        super(props);

        // 紀錄id
        this.index = this.props.index;
    }


    componentWillUnmount(){}


    render()
    {
        let text = this.props.showText? this.props.text : "";
        let width = this.props.width || 0;
        let height = this.props.height || 0;

        let style = 
          {
              backgroundColor : this.props.color,
              width : width,
              height : height,
              lineHeight : height + 'px'
          }
        style = Object.assign(style, this.getAdditionalStyle());
        let onClickFunc = this.getOnClickFunc();
        let onMouseOverFunc = this.getOnMouseOverFunc();
        let onMouseOutFunc = this.getOnMouseOutFunc();
        
        return (
          <div className="grid" style={style} onMouseEnter={onMouseOverFunc} onMouseLeave={onMouseOutFunc} 
          onClick={onClickFunc}>
            {text}
          </div> 
        );
    }

    
    // 需要給繼承 class override 的方法
    getOnClickFunc(){}
    getOnMouseOverFunc(){}
    getOnMouseOutFunc(){}
    getAdditionalStyle(){}

}

export default Grid;

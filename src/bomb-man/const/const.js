// 定義關卡名稱
const map_type = 
  {
      NORMAL    : 0,
      NORMAL_2  : 1,
      NORMAL_3  : 2,
      LENGTH    : 3
  };

// 定義補給品種類
const supply_type = 
  {
      BLAST_RANGE : "BLAST_RANGE",
      BOMB_NUM : "BOMB_NUM",
      LIFE_UP : "LIFE_UP",
      CHAOS : "CHAOS",
      SUPER_BOMB : "SUPER_BOMB"
  }

// 定義補給品顏色
const supply_color = 
  {
      [supply_type.BLAST_RANGE] : "orange",
      [supply_type.BOMB_NUM] : "black",
      [supply_type.LIFE_UP] : "red",
      [supply_type.CHAOS] : "purple",
      [supply_type.SUPER_BOMB] : "#FFFF00"
  }

// 定義補給品文字
const supply_text = 
  {
      [supply_type.BLAST_RANGE] : "P",
      [supply_type.BOMB_NUM] : "B",
      [supply_type.LIFE_UP] : "L",
      [supply_type.CHAOS] : "C",
      [supply_type.SUPER_BOMB] : "S"
  }

// 定義能力種類
const ability_type =
  {
      BOMB_NUM : "BOMB_NUM",
      LIFE : "LIFE",
      BLAST_RANGE : "BLAST_RANGE"
  }
 
// 定義基本障礙
const basic_block = 
  [
      24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44,
      70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90,
      116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136,
      162, 164, 166, 168, 170, 172, 174, 176, 178, 180, 182,
      208, 210, 212, 214, 216, 218, 220, 222, 224, 226, 228
  ];

// 定義關卡內容
// @ block : 不可破壞障礙
// @ block_destructible : 可破壞障礙
// @ supply : 補給品資訊
const map_data = 
{   

    // 普通
    [map_type.NORMAL] :
    {
            block : basic_block,
            block_destructible :
                [
                    2, 3, 4, 5, 6, 25, 48, 27, 49, 50, 53, 46, 47, 71, 94, 95, 73, 96, 117, 140, 163, 141, 142, 
                    143, 144, 145, 146, 7, 8, 9, 31, 54,
                    184, 185, 186, 207, 230, 231, 232, 209, 119, 233, 234, 235, 211, 138, 139, 161,
                    102, 125, 148, 171, 194, 147, 79, 217, 55, 56, 33, 195, 196, 197, 57, 58, 59, 81, 191, 192, 215, 238, 
                    239, 14, 15, 37, 61, 62, 85,
                    206, 250, 249, 248, 247, 246, 245, 244, 243, 221, 198, 199, 225, 227, 202, 203, 204, 205, 179, 181, 156, 157, 158, 133, 135,
                    110, 111, 112, 89, 106, 107, 108, 109, 66, 43, 20, 21, 22, 45, 67, 68, 41, 17, 18, 19, 91 , 113, 114, 
                ],
            supply : 
                {
                    [supply_type.BLAST_RANGE] : 10,
                    [supply_type.BOMB_NUM] : 10,
                    [supply_type.LIFE_UP] : 4,
                    [supply_type.CHAOS] : 0,
                    [supply_type.SUPER_BOMB] : 2,
                }
    },

    // 方形中空
    [map_type.NORMAL_2] :
    {
            block : 
                [
                    24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44,
                    70, 72, 74, 76, 84, 86, 88, 90,
                    116, 118, 120, 122, 130, 132, 134, 136,
                    162, 164, 166, 168, 176, 178, 180, 182,
                    208, 210, 212, 214, 216, 218, 220, 222, 224, 226, 228
                ], 
            block_destructible :
                [
                    46, 47, 49, 69, 71, 92, 93, 94, 95, 3, 27, 50, 73, 96, 48, 119, 142, 143, 165,
                    139, 117, 163, 141, 5, 6, 29, 140, 230, 231, 232, 236, 237, 184, 185, 186, 187, 
                    188, 189, 190, 207, 209, 213, 4, 2, 25, 191, 144, 145, 167, 52, 53, 54, 75, 98, 99, 100,

                    78, 80, 82, 102, 104, 124, 126, 128, 148, 150, 170, 172, 174, 54, 60, 192, 198, 215, 238,

                    246, 247, 249, 225, 202, 203, 204, 205, 206, 179, 181, 183, 156, 157, 158, 159, 160,
                    133, 135, 109, 110, 111, 113, 87, 89, 62, 63, 64, 65, 66, 67, 68, 43, 45, 20, 21, 22,
                    14, 15, 16, 37, 39, 112, 248, 227, 250, 61, 85, 107, 108, 152, 153, 154, 199, 200, 177, 223
                ],
            supply : 
                {
                    [supply_type.BLAST_RANGE] : 10,
                    [supply_type.BOMB_NUM] : 10,
                    [supply_type.LIFE_UP] : 6,
                    [supply_type.CHAOS] : 4,
                    [supply_type.SUPER_BOMB] : 2,
                }
    },

    // Made By Tim
    [map_type.NORMAL_3] :
    {
            block : 
                [
                    24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44,
                    70, 72, 74, 76, 84, 86, 88, 90,
                    116, 118, 120, 122, 130, 132, 134, 136,
                    162, 164, 166, 168, 176, 178, 180, 182,
                    208, 210, 212, 214, 216, 218, 220, 222, 224, 226, 228,
                    47, 185, 27, 119, 211, 75, 167, 55, 147, 103, 195, 59, 151, 39, 131, 223, 87, 179, 67, 205
                ],
            block_destructible :
                [
                    // 2, 3, 4, 5, 6, 25, 48, 27, 49, 50, 53, 46, 47, 71, 94, 95, 73, 96, 117, 140, 163, 141, 142, 
                    // 143, 144, 145, 146, 7, 8, 9, 31, 54,
                    // 184, 185, 186, 207, 230, 231, 232, 209, 119, 233, 234, 235, 211, 138, 139, 161,
                    // 102, 125, 148, 171, 194, 147, 79, 217, 55, 56, 33, 195, 196, 197, 57, 58, 59, 81, 191, 192, 215, 238, 
                    // 239, 14, 15, 37, 61, 62, 85,
                    // 206, 250, 249, 248, 247, 246, 245, 244, 243, 221, 198, 199, 225, 227, 202, 203, 204, 205, 179, 181, 156, 157, 158, 133, 135,
                    // 110, 111, 112, 89, 106, 107, 108, 109, 66, 43, 20, 21, 22, 45, 67, 68, 41, 17, 18, 19, 91 , 113, 114, 
                ],
            supply : 
                {
                    [supply_type.BLAST_RANGE] : 10,
                    [supply_type.BOMB_NUM] : 10,
                    [supply_type.LIFE_UP] : 4,
                    [supply_type.CHAOS] : 0,
                    [supply_type.SUPER_BOMB] : 2,
                }
    }
}

// 定義角色能力細節 
const ability = 
{
    // 炸彈基本數量
    basic_bomb_num : 1,

    // 炸彈最大數量
    max_bomb_num : 6,

    // 炸彈基本威力
    basic_blast_range : 1,

    // 炸彈最大威力
    max_blast_range : 6,

    // 基本血量
    basic_life : 2,

    // 最大血量
    max_life : 3,

    // 炸後無敵時間(ms)
    invuncible_duration_after_harm : 1500,

    // 混亂時間
    chaos_duration : 3500,

    // 超級炸彈威力範圍
    super_bomb_blast_range : 100,
}



export {map_data, map_type, supply_type, supply_text, supply_color, ability, ability_type};
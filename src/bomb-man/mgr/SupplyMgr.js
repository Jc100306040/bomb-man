// 補給品管理器

import { supply_type, supply_color, supply_text } from '../const/const.js'; 

class SupplyMgr
{

    // @ block_arr : 所有障礙物的index
    // @ supply_info : 補給品的詳細資訊
    constructor(block_arr, supply_info)
    {
        let supplyDataArr = [];
        let blockArr = block_arr.slice();

        for(let type in supply_info)
        {
            let supplyDataArr_Per = [];
            let supply_num = supply_info[type];
            let color = null;

            // 為避免重複index，先把已經篩選出來的給過濾掉
            if(supplyDataArr.length)
            {
                let compareArr = supplyDataArr.map( x => x.index);
                blockArr = blockArr.filter(x => compareArr.indexOf(x)==-1);
            }
            
            supplyDataArr_Per = this.getArrElementByRandom(blockArr, supply_num, supplyDataArr_Per);
            // console.log(supplyDataArr_Per);

            supplyDataArr_Per = supplyDataArr_Per.map( x => {
              return {index:x, color:supply_color[type], type:type, text:supply_text[type]};
            });
            supplyDataArr = supplyDataArr.concat(supplyDataArr_Per);
        }

        // console.log(supplyDataArr);
        this.supplyData = supplyDataArr;
    }


    getSupplyData(){ return this.supplyData; }


    // 從某矩陣中取得指定數量的隨機元素
    // @ arr : 指定矩陣
    // @ num : 指定數量
    // @ curArr : 結果容器，參數可遞回使用
    getArrElementByRandom(arr, num, curArr)
    {
        if(curArr.length + arr.length < num) return curArr;
        if(curArr.length >= num) return curArr;

        let result_arr = curArr.slice();
        let randomIndex = Math.floor(Math.random()*arr.length);
        let randomValue = arr[randomIndex];

        result_arr.indexOf(randomValue)==-1 && result_arr.push(randomValue);

        if(result_arr.length != num)
        {
            return this.getArrElementByRandom(arr, num, result_arr);
        }

        return result_arr;
    }
}

export default SupplyMgr;

// 炸彈管理器

class BombMgr
{

    // @ playerID : 玩家ID
    // @ bombMaxNum : 炸彈放置最大數量
    constructor(playerID, bombMaxNum)
    {
        // 玩家ID
        this.playerID = playerID;

        // 炸彈最大數量
        this.bombMaxNum = bombMaxNum;

        // 已放炸彈數量
        this.bombExist = 0;

        // 已放炸彈之index
        this.bombIndexArr = [];
    }


    // 判斷是否可以放炸彈
    canSetBomb()
    {
        return this.bombMaxNum - this.bombExist > 0;
    }


    // 放炸彈
    setBomb(index)
    {
        // 防呆，判斷是否已經存在
        if(this.bombIndexArr.indexOf(index)!=-1) return;
        this.bombIndexArr.push(index);
        this.bombExist ++;
    }


    // 炸彈爆炸
    refreshBombArrWhenBlast(arr)
    {
        this.bombIndexArr = this.bombIndexArr.filter(x => arr.indexOf(x)!=-1);
        this.bombExist = this.bombIndexArr.length;
    }


    // 刷新炸彈最大數量
    refreshBombNumLevel(maxNumber)
    {
        if(this.bombMaxNum >= maxNumber) return;
        this.bombMaxNum = maxNumber;
    } 

}

export default BombMgr;

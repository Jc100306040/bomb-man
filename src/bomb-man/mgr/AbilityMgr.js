// 角色能力管理器

import { ability } from '../const/const.js'; 

class AbilityMgr
{

    // @ playerData : 角色能力資訊
    constructor(playerID)
    {
        this.playerID = playerID;

        let basic_ability = 
        {
            blast_range_level : ability.basic_blast_range,
            bomb_num_level : ability.basic_bomb_num,
            life : ability.basic_life
        };

        this.playerData = basic_ability;
    }


    // 取得資訊
    getPlayerData(){ return this.playerData; }


    // 炸彈數量提升
    bombNumLevelUp()
    {
        if(!this.playerData.bomb_num_level) return; 
        if(this.playerData.bomb_num_level >= ability.max_bomb_num) return;
        this.playerData.bomb_num_level ++;
    }


    // 炸彈威力提升
    blastLevelUp()
    {
        if(!this.playerData.blast_range_level) return; 
        if(this.playerData.blast_range_level >= ability.max_blast_range) return;
        this.playerData.blast_range_level ++;
    }


    // 血量減少
    lifeDown()
    {
        if(!this.playerData.life) return; 
        if(this.playerData.life <= 0) return;
        this.playerData.life --;
    }


    // 血量增加
    lifeUp()
    {
        if(!this.playerData.life) return; 
        if(this.playerData.life >= ability.max_life) return;
        this.playerData.life ++;
    }

}

export default AbilityMgr;

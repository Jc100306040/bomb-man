// 角色

import React, { Component } from 'react';
import { supply_type, ability } from './const/const.js'; 

class Character extends Component 
{

    constructor(props)
    {
        super(props);

        // 定義玩家按鍵組
        this.keyBoardSetting = 
        {
            
            // 玩家1
            1 : 
            {
            "up" : "KeyW",
            "down" : "KeyS",
            "right" : "KeyD",
            "left" : "KeyA",
            "bomb" : "KeyQ"
            },

            // 玩家2
            2 : 
            {
            "up" : "ArrowUp",
            "down" : "ArrowDown",
            "right" : "ArrowRight",
            "left" : "ArrowLeft",
            "bomb" : "Space"
            }
        }

        // 定義玩家顏色
        this.backgroundColorSetting = 
        {
            // 玩家1 
            1 : "#00DD00",
            
            // 玩家2
            2 : "#00BBFF"
        }

        // 定義玩家起始位置
        this.startIndex = 
        {
            // 玩家1 
            1 : 0,
            
            // 玩家2
            2 : 252
        }

        // 玩家編號
        this.playerID = this.props.playerID; 

        this.state = 
        {
            gridIndex : this.startIndex[this.props.playerID],
            left : this.getLeft(this.startIndex[this.props.playerID]),
            top  : this.getTop(this.startIndex[this.props.playerID]),
            backgroundColor : this.backgroundColorSetting[this.props.playerID],
            opacity : 1
        };

        // function
        this.setBomb = this.props.setBomb;
        this.getSupply = this.props.getSupply;
        this.refreshAbility = this.props.refreshAbility;

        // 存活狀態
        this.alive = true;

        // BombMgr
        this.bombMgr = this.props.bombMgr;

        // AbilityMgr
        this.abilityMgr = this.props.abilityMgr;

        // 爆炸範圍等級
        this.blast_range_level = this.abilityMgr.getPlayerData().blast_range_level;

        // 炸彈數量
        this.bomb_num_level = this.abilityMgr.getPlayerData().bomb_num_level;

        // 血量
        this.life = this.abilityMgr.getPlayerData().life;

        // 角色吃到補品後最大血量(計算alpha用，只加不減)
        this.max_life = this.life;

        // 是否進入無敵時間
        this.isInInvincibleTime = false;

        // 是否進入混亂時間
        this.isChaos = false;

        // 超級炸彈獲得量
        this.super_bomb_num = 0;

        // 設置鍵盤輸入
        this.setKeyBoardEvent();
    }


    // 設置鍵盤事件
    setKeyBoardEvent()
    {
        // 這邊要先用 bind 的方式暫存起來，才能 remove 掉
        this.onKeyBoardEventBind = this.onKeyBoardEvent.bind(this);
        document.addEventListener('keydown', this.onKeyBoardEventBind);
    }


    // 鍵盤事件
    onKeyBoardEvent(e)
    {
        if(!this.alive) return;

        let row = this.props.boardData.row;
        let col = this.props.boardData.col;
        let placeNeedBlock = this.props.boardData.placeNeedBlock;
        let tempGridIndex = this.state.gridIndex;

        // 檢查是否在邊界
        let isMaxLeft   = this.state.gridIndex%col == 0;
        let isMaxRight  = (this.state.gridIndex+1)%col == 0;
        let isMaxTop    = this.state.gridIndex - col < 0;
        let isMaxBottom = this.state.gridIndex + col >= col*row;

        let id = this.playerID;
        let keyBoardSetting = this.keyBoardSetting[id];

        // 判斷方向是否被混亂狀態影響
        let keyBoardDirection = (this.isChaos)? this.getChaosDirection(e.code) : e.code;    

        switch(keyBoardDirection)
        {
            // 移動
            case keyBoardSetting["up"]:
                if(isMaxTop) return;
                this.state.gridIndex -= col;
                break;

            case keyBoardSetting["down"]:
                if(isMaxBottom) return;
                this.state.gridIndex += col;
                break;

            case keyBoardSetting["right"]:
                if(isMaxRight) return;
                this.state.gridIndex += 1;
                break; 

            case keyBoardSetting["left"]:
                if(isMaxLeft) return;
                this.state.gridIndex -= 1;
                break; 

            // 放炸彈
            case keyBoardSetting["bomb"]:

                // 判斷是放超級炸彈或一般炸彈
                if(this.super_bomb_num)
                {
                    this.super_bomb_num --;
                    this.setBomb(this.state.gridIndex, ability.super_bomb_blast_range);
                }
                // 若為一般炸彈，則判斷剩餘數量是否足夠
                else if(this.bombMgr.canSetBomb())
                {
                    this.setBomb(this.state.gridIndex, this.blast_range_level);
                    this.bombMgr.setBomb(this.state.gridIndex);
                }

                break;
        }

        //  檢查是否有撞牆
        if(placeNeedBlock.indexOf(this.state.gridIndex)!=-1)
        {
            this.state.gridIndex = tempGridIndex;
            return;
        }

        // 設置玩家位置
        this.setPosition(this.state.gridIndex);

        // 判斷是否有獲得補給品
        this.checkIsGetSupply(this.state.gridIndex);
    }


    // 判斷是否有取得補給品
    checkIsGetSupply(index)
    {
        let supplyIndexArr = this.props.boardData.supply.map( x => x.index);
        if(supplyIndexArr.indexOf(index)!=-1)
        {
            // 若有，取得補給品資訊
            let supplyDetail = this.props.boardData.supply.filter( x => x.index==index);
            let type = supplyDetail[0].type;

            switch(type)
            {
                // ====== 爆炸威力 ======
                case supply_type.BLAST_RANGE:

                    this.abilityMgr.blastLevelUp();
                    this.blast_range_level = this.abilityMgr.getPlayerData().blast_range_level;

                    break;

                // ====== 炸彈數量 ======
                case supply_type.BOMB_NUM:
                    
                    this.abilityMgr.bombNumLevelUp();
                    this.bomb_num_level = this.abilityMgr.getPlayerData().bomb_num_level;

                    // 刷新炸彈bombMgr中的炸彈最大數量
                    this.bombMgr.refreshBombNumLevel(this.bomb_num_level);
                    break;

                // ====== 生命提升 ======
                case supply_type.LIFE_UP:
                    
                    this.abilityMgr.lifeUp();
                    this.life = this.abilityMgr.getPlayerData().life;

                    // 判斷是否已超出角色最大血量，若有，則刷新
                    this.max_life = this.life > this.max_life ? this.life : this.max_life;

                    this.setOpacity(this.life / this.max_life);

                    break;

                // ====== 混亂狀態 ======
                case supply_type.CHAOS:
                    
                    if(this.isChaos) break;
                    this.setIsChaos(true);

                    // 設置狀態時間
                    setTimeout(()=>{
                        this.setIsChaos(false);
                    }, ability.chaos_duration);
                    
                    break;

                // ====== 超級炸彈 ======
                case supply_type.SUPER_BOMB:

                    this.super_bomb_num ++;

                    break;
            }

            this.getSupply(index);
            this.refreshAbility(this.playerID, this.abilityMgr.getPlayerData());
        }
    }


    // 取得混亂狀態後的按鍵輸入
    getChaosDirection(keyCode)
    {
        let result = keyCode;
        let id = this.playerID;
        let keyBoardSetting = this.keyBoardSetting[id];

        switch(keyCode)
        {
            // 移動
            case keyBoardSetting["up"]:
            result = keyBoardSetting["down"];
            break;

            case keyBoardSetting["down"]:
            result = keyBoardSetting["up"];
            break;

            case keyBoardSetting["right"]:
            result = keyBoardSetting["left"];
            break; 

            case keyBoardSetting["left"]:
            result = keyBoardSetting["right"];
            break;
        }

        return result;
    }


    setIsChaos(isChaos)
    {
        this.isChaos = isChaos;

        let backgroundColor = isChaos? "#66009D" : this.backgroundColorSetting[this.playerID];
        this.setState({backgroundColor:backgroundColor});
    }


    setPosition(index)
    {
        this.setState({
            top   : this.getTop(index),
            left  : this.getLeft(index),
        });
    }


    setOpacity(opacity)
    {
        this.setState({opacity:opacity});
    }


    getLeft(index)
    {
        return (index % this.props.boardData.col)*50;
    }


    getTop(index)
    {
        return Math.floor(index/this.props.boardData.col)*50;
    }


    componentDidUpdate()
    {
        // 判斷是否有被炸到
        let blastRange = this.props.boardData.blastEffect[0];
        
        if(!blastRange) return;
        if(this.isInInvincibleTime) return;

        if(blastRange.indexOf(this.state.gridIndex) != -1 && this.alive)
        {
            this.abilityMgr.lifeDown();
            this.life = this.abilityMgr.getPlayerData().life;

            this.isInInvincibleTime = true;

            // 損血後透明度變化
            this.setOpacity(this.life / this.max_life);

            if(!this.life)
            {
                this.alive = false;
                return;
            } 

            // 無敵狀態計時器
            setTimeout(()=>{
                this.isInInvincibleTime = false;
            }, ability.invuncible_duration_after_harm);
        }
    }


    render()
    {
        return (
        <div className="character" style={{backgroundColor:this.state.backgroundColor, opacity:this.state.opacity,
            top:this.state.top, left:this.state.left}}></div>
        );
    }


    componentWillUnmount()
    {
        // 取消註冊鍵盤事件
        document.removeEventListener('keydown', this.onKeyBoardEventBind);
    }

}

export default Character;

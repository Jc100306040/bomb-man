// 遊戲介面：炸彈人

import React, { Component } from 'react';
import Grid from '../common/Grid.js';
import Supply from './Supply.js';
import Character from './Character.js';
import BoardState from './BoardState.js';
import SupplyMgr from './mgr/SupplyMgr.js';
import BombMgr from './mgr/BombMgr.js';
import AbilityMgr from './mgr/AbilityMgr.js';
import {map_data, map_type, supply_type, ability } from './const/const.js'; 

class BoardMain extends Component 
{


  constructor(props)
  {
    super(props);

    // 地圖設置
    let randomMapIndex = Math.floor( Math.random() * map_type.LENGTH);
    randomMapIndex = 2;
    this.block = map_data[randomMapIndex].block;
    let block_destruc = map_data[randomMapIndex].block_destructible;
    let placeNeedBlock = this.block.concat(block_destruc);

    // 設置補給品管理器
    let supplyDetail = map_data[randomMapIndex].supply;
    this.supplyMgr = new SupplyMgr(block_destruc, supplyDetail);
    
    // 設置炸彈管理器
    this.bombMgr_1 = new BombMgr(1, ability.basic_bomb_num);
    this.bombMgr_2 = new BombMgr(2, ability.basic_bomb_num);

    // 設置角色能力管理器
    this.abilityMgr_1 = new AbilityMgr(1);
    this.abilityMgr_2 = new AbilityMgr(2);

    // 狀態初始化
    this.state = 
    {
        // 可破壞障礙物 
        block_destruc : block_destruc,

        // 所有不可通過之障礙
        placeNeedBlock : placeNeedBlock,

        // 炸彈位置
        placePutBomb :[],

        // 黃金炸彈位置
        placePutSuperBomb : [],

        // 爆炸效果緩存池
        blastEffectQ : [],

        // 計時器緩存池( blastEffectQ Index : timoutNumber )
        timerQ :[],

        // 爆炸效果
        blastEffect : [],

        // 補給品
        supply : this.supplyMgr.getSupplyData(),

        // 玩家狀態
        player_1_data : this.abilityMgr_1.getPlayerData(),
        player_2_data : this.abilityMgr_2.getPlayerData()
    };

    this.col = 23;
    this.row = 11;
    this.bombDuration = 2500;
    this.blastDuration = 100;
    this.gridWidth = 50;
    this.supplyWidth = 25;
    this.gridWidth = this.gridHeight = 50;

    // 測試用變數
    this.showText = false;
    this.showSupplyPosition = false;
  }


  // 畫出狀態面板
  drawStateBoard()
  {
     return (<BoardState 
      player_1_data={this.state.player_1_data} player_2_data={this.state.player_2_data}/>);
  }


  // 畫出補給品
  drawSupplys()
  {
     let supplys = [];

     for(let i=0 ; i<this.state.supply.length ; i++)
     {
        let index = this.state.supply[i].index;

        // 判斷目前是否有障礙，有障礙就不畫出
        if(!this.showSupplyPosition)
        {
          let block_destruc = this.state.block_destruc;
          if(block_destruc.indexOf(index)!=-1) continue;
        }
        
        let color = this.state.supply[i].color;
        let text = this.state.supply[i].text;
        let left = (index % this.col)*this.gridWidth + (this.gridWidth-this.supplyWidth)/2;
        let top = Math.floor(index/this.col)*this.gridWidth + (this.gridWidth-this.supplyWidth)/2;

        supplys.push( <Supply color={color} left={left} top={top} text={text}/> );
     }

     return supplys;
  }


  // 畫出爆炸、炸彈、障礙
  drawBlocks()
  {
     let grids = [];
     let number = this.col * this.row;
     for(let i=0 ; i<number ; i++)
     {
        let color = this.state.placeNeedBlock.indexOf(i) == -1 ? "white" : 
          this.state.block_destruc.indexOf(i) == -1 ? "#8E8E8E" : "#844200"; 
        
        //  檢查是否為炸彈
        if(this.state.placePutBomb.indexOf(i) != -1) color = "black";
        
        // 判斷是否為超級炸彈
        if(this.state.placePutSuperBomb.indexOf(i) != -1) color = "#FFFF00";
        
        //  檢查是否為爆炸效果
        let firstQ = this.state.blastEffect[0];
        if(firstQ)
        {
            if(firstQ.indexOf(i) != -1) color = "#FFBB00" ;
        }

        grids.push(
          <Grid color={color} text={i} showText={this.showText}
            width={this.gridWidth} height={this.gridHeight}/>
          );
     }
     return grids;
  }


  // 設置角色
  drawCharacter(id)
  {
    // 設置場地資訊
    let boardData = 
    {
      placeNeedBlock : this.state.placeNeedBlock,
      blastEffect : this.state.blastEffect,
      supply : this.state.supply,
      col : this.col,
      row : this.row
    };

    // 炸彈管理器
    let bombMgr = id==1? this.bombMgr_1 : this.bombMgr_2; 

    // 角色能力管理器
    let abilityMgr = id==1? this.abilityMgr_1 : this.abilityMgr_2;

    return <Character playerID={id} boardData={boardData} bombMgr={bombMgr} abilityMgr={abilityMgr}
      setBomb={this.setBomb.bind(this)} getSupply={this.detroySupply.bind(this)} refreshAbility={this.refreshAbility.bind(this)}/>; 
  }


  // 刷新玩家資訊
  refreshAbility(id, playerData)
  {
      if(id==1)
      {
          this.setState({ player_1_data : playerData});
      }
      else
      {
          this.setState({ player_2_data : playerData});
      }
  }
  

  // 取得(破壞)補給品
  detroySupply(index)
  {
      let supply = this.state.supply.slice();
      supply = supply.filter( x => x.index!=index);
      this.setState({ supply : supply});
  }


  // 設置炸彈 
  setBomb(index, power)
  {

    console.log("Set Bomb", index, power);

    let placeNeedBlock = this.state.placeNeedBlock.slice();
    let placePutBomb = this.state.placePutBomb.slice();
    let placePutSuperBomb = this.state.placePutSuperBomb.slice();

    // 判斷這個位置是否已有障礙
    if(placeNeedBlock.indexOf(index) != -1) return;

    placeNeedBlock.push(index);

    // 判斷是否為超級炸彈
    if(power==ability.super_bomb_blast_range)
    {
        placePutSuperBomb.push(index);
    }
    else
    {
        placePutBomb.push(index);
    }

    this.setState({ placeNeedBlock : placeNeedBlock, placePutBomb : placePutBomb, placePutSuperBomb : placePutSuperBomb});

    // 判斷新增炸彈是否有在已存在炸彈之爆炸範圍中
    let isBombEffectByBlast = this.checkAndHandleBombEffectByBlast(index, power);

    // 無在爆炸範圍，自行爆炸
    if(!isBombEffectByBlast)
    {
        // 將爆炸範圍置入緩存池，等待爆發
        let blastArr = this.getBlastIndexs(index, power);
        let blastEffectQ = this.state.blastEffectQ.slice();
        blastEffectQ.push(blastArr);   
        
        // 設置延遲爆炸計時器
        let timer = setTimeout(()=>{

          // 炸彈自毀
          this.clearBomb(index);

          // 爆炸效果
          this.playBlastEffect();

        }, this.bombDuration);
        
        // 將計時器放入緩存池
        let timerQ = this.state.timerQ.slice();
        let data = {};
        data.blastEffectQ = blastArr;
        data.timer = timer;
        timerQ.push(data);

        this.setState({ 
          blastEffectQ : blastEffectQ,
          timerQ : timerQ
        });
    }
  }
 

  // 炸彈自毀
  clearBomb(index)
  {
    let placeNeedBlock = this.state.placeNeedBlock.slice();
    let block_destruc = this.state.block_destruc.slice();
    let placePutBomb = this.state.placePutBomb.slice();
    let placePutSuperBomb = this.state.placePutSuperBomb.slice();

    if(placePutBomb.indexOf(index) == -1) return;

    placeNeedBlock = placeNeedBlock.filter(x => {return x!=index});
    block_destruc = block_destruc.filter(x => {return x!=index});
    placePutBomb = placePutBomb.filter(x => {return x!=index});
    placePutSuperBomb = placePutSuperBomb.filter(x => {return x!=index});

    // 刷新炸彈管理器
    this.setBombMgrWhenBlast(placePutBomb);

    this.setState({ 
      placeNeedBlock : placeNeedBlock, 
      block_destruc : block_destruc, 
      placePutBomb : placePutBomb,
      placePutSuperBomb : placePutSuperBomb
    });
  }
  

  // 爆炸效果
  playBlastEffect()
  {
      // 判斷是否有在緩存池等待排隊的爆炸
      let blastEffectQ = this.state.blastEffectQ.slice();
      let blastEffectArr = blastEffectQ.shift();

      if(!blastEffectArr || !blastEffectArr.length) return;

      // 爆炸效果展示
      let blastEffect = this.state.blastEffect.slice();
      blastEffect.push(blastEffectArr);

      // 爆炸會把障礙給炸掉，故要多處理障礙arr
      let block_destruc = this.state.block_destruc.slice();
      block_destruc = block_destruc.filter(x => blastEffectArr.indexOf(x)==-1);

      let placeNeedBlock = this.state.placeNeedBlock.slice();
      // 爆炸不是每個東西都能炸，所以要把影響index先篩選出來
      let blastEffectArrAfterFilter = blastEffectArr.filter(x => this.block.indexOf(x)==-1);
      placeNeedBlock = placeNeedBlock.filter(x => blastEffectArrAfterFilter.indexOf(x)==-1);

      // 爆炸也會把在範圍內炸彈給炸掉，故也要處理炸彈arr
      let placePutBomb = this.state.placePutBomb.slice();
      placePutBomb = placePutBomb.filter(x => blastEffectArr.indexOf(x)==-1);

      let placePutSuperBomb = this.state.placePutSuperBomb.slice();
      placePutSuperBomb = placePutSuperBomb.filter(x => blastEffectArr.indexOf(x)==-1);

      // 爆炸會破壞補給品(有牆，尚未現形的話，則不會被破壞)
      let supplyDestroyArr = blastEffectArr.slice().filter(x => this.state.block_destruc.slice().indexOf(x)==-1);
      for(let i=0 ; i<supplyDestroyArr.length ; i++){ this.detroySupply(supplyDestroyArr[i]);}

      // 刷新炸彈管理器
      this.setBombMgrWhenBlast(placePutBomb);

      // 爆炸了，清除計時器
      let timerQ = this.state.timerQ.slice();
      timerQ = timerQ.filter(x => x.blastEffectQ != blastEffectArr);
      
      console.log("Blast Effect ->", blastEffect[0]);

      this.setState({ 
        blastEffectQ : blastEffectQ,
        blastEffect : blastEffect,
        placeNeedBlock : placeNeedBlock,
        block_destruc : block_destruc,
        placePutBomb : placePutBomb,
        placePutSuperBomb : placePutSuperBomb,
        timerQ : timerQ
      });
      
      // 爆炸效果自毀
      setTimeout(()=>{
          let blastEffect = this.state.blastEffect.slice();
          blastEffect.shift();
          this.setState({ blastEffect : blastEffect });
    }, this.blastDuration);
  }


  // 判斷炸彈是否有在已存在炸彈之爆炸範圍中，
  // 若有，將新炸彈之爆炸範圍也加入舊炸彈之爆炸範圍中，同時引爆
  checkAndHandleBombEffectByBlast(index, power)
  {

    let blastEffectQ = this.state.blastEffectQ.slice();

    // 如果還沒有緩存紀錄，則回傳false
    if(!blastEffectQ.length) return false;

    // 判斷是否有會影響到此炸彈的緩存爆炸範圍
    let blastsAffectByBomb = [];
    let firstBlastEffectIndex = null;

    for(let i=0; i<blastEffectQ.length; i++)
    {
        let perBlastArr = blastEffectQ[i];
        if(perBlastArr.indexOf(index) != -1)
        {
            // 合併範圍
            blastsAffectByBomb = blastsAffectByBomb.concat(perBlastArr);

            // 判斷是否為第一組範圍，如果是就存取index，之後做資料整合時使用
            // 若否，則移除timer
            if(firstBlastEffectIndex == null) 
            {
                firstBlastEffectIndex = i;
            }
            else
            {
                let timerQ = this.state.timerQ.slice();

                timerQ = timerQ.filter(x => {
                  if(this.isArraysEqual(perBlastArr, x.blastEffectQ))
                  {
                     clearTimeout(x.timer);
                     return false;
                  }
                  return true;
                });

                this.setState({ timerQ : timerQ });
            }
        }
    }

    // 如果都沒有，則回傳false
    if(!blastsAffectByBomb.length) return false;

    // 開始處理資料，先將此炸彈之爆炸範圍加入爆炸區，為避免重複index，要先過濾
    blastsAffectByBomb = blastsAffectByBomb.concat(this.getBlastIndexs(index, power));

    let filterFunction = (value, index, arr) => { 
      return arr.indexOf(value) === index;
    }

    // 此為合併後資料
    let blastsAfterChain = blastsAffectByBomb.filter(filterFunction);

    // 開始處理原始之 blastEffectQ
    blastEffectQ = blastEffectQ.filter( x => x.indexOf(index)==-1);
    blastEffectQ.splice(firstBlastEffectIndex, 0, blastsAfterChain);

    // 資料設置
    this.setState({ blastEffectQ : blastEffectQ });

    return true;
  }


  // 炸彈爆炸時對管理器進行刷新
  setBombMgrWhenBlast(placePutBomb)
  {
     this.bombMgr_1.refreshBombArrWhenBlast(placePutBomb);
     this.bombMgr_2.refreshBombArrWhenBlast(placePutBomb);
  }


  // getBlastIndexs_old(index)
  // {
  //   let power = 5;
  //   let block = this.block;
  //   let block_destruc = this.state.block_destruc;

  //   // 判斷是否為障礙物
  //   let isBlock = (x) => { return block.indexOf(x)!=-1; }

  //   // 判斷是否為可破壞障礙物
  //   let isBlockDestruc =  (x) => { return block_destruc.indexOf(x)!=-1; }

  //   // 先推入炸彈本身index
  //   let arr = [];
  //   arr.push(index);

  //   // 如遇到可破壞之障礙物，爆炸範圍只能+1
  //   // 如遇到不可破壞之障礙物，爆炸範圍不可繼續增加

  //   // 先定義現在的上下左右位置，由於一開始還沒向外展開，故默認為中心點
  //   let now_left, now_right, now_top, now_bottom;
  //   now_left = now_right = now_top = now_bottom = index;

  //   // 判斷是否已經破壞已經障礙物了
  //   let isDestroyBlock_left, isDestroyBlock_right, isDestroyBlock_top, isDestroyBlock_bottom;
  //   isDestroyBlock_left = isDestroyBlock_right = isDestroyBlock_top = isDestroyBlock_bottom = false;

  //   for(let i=0; i<power ; i++)
  //   {
  //       // 先檢查目前是否在邊界，如果現在位置是null，代表已超出邊界
  //       let isMaxLeft   = now_left? now_left % this.col == 0 : true;
  //       let isMaxRight  = now_right? (now_right + 1) % this.col == 0 : true;
  //       let isMaxTop    = now_top? now_top - this.col < 0 : true;
  //       let isMaxBottom = now_bottom? now_bottom + this.col >= this.col * this.row : true;

  //       // console.log(isMaxTop, isMaxRight, isMaxBottom, isMaxLeft);

  //       // 接著展開的上下左右位置
  //       let j = i+1;
  //       let nxt_left   = isMaxLeft || isDestroyBlock_left? null : index - 1*j;
  //       let nxt_right  = isMaxRight || isDestroyBlock_right? null : index + 1*j;
  //       let nxt_top    = isMaxTop || isDestroyBlock_top ? null : index - this.col*j;
  //       let nxt_bottom = isMaxBottom || isDestroyBlock_bottom? null : index + this.col*j;
        
  //       // 判斷是否為不可破壞之障礙物
  //       if(isBlock(nxt_left)) nxt_left = null;
  //       if(isBlock(nxt_right)) nxt_right = null;
  //       if(isBlock(nxt_top)) nxt_top = null;
  //       if(isBlock(nxt_bottom)) nxt_bottom = null;

  //       // 判斷是否為可破壞障礙物
  //       if(isBlockDestruc(nxt_left)) isDestroyBlock_left = true;
  //       if(isBlockDestruc(nxt_right)) isDestroyBlock_right = true;
  //       if(isBlockDestruc(nxt_top)) isDestroyBlock_top = true;
  //       if(isBlockDestruc(nxt_bottom)) isDestroyBlock_bottom = true;

  //       // console.log(nxt_top, nxt_right, nxt_bottom, nxt_left);

  //       // 刷新現在的上下左右位置，如果已經在邊界，則設為null
  //       now_left   = isMaxLeft? null : nxt_left;
  //       now_right  = isMaxRight? null : nxt_right;
  //       now_top    = isMaxTop? null : nxt_top;
  //       now_bottom = isMaxBottom? null : nxt_bottom;

  //       nxt_right!=null && arr.push(nxt_right);
  //       nxt_bottom!=null && arr.push(nxt_bottom);
  //       nxt_left!=null && arr.push(nxt_left);
  //       nxt_top!=null && arr.push(nxt_top);
  //   }

  //   return arr;
  // }


  // 取得爆炸範圍index arr
  // 未來，我將看不懂這段程式碼
  // 但此刻，我覺得我是神
  getBlastIndexs(index, power)
  {
    let block = this.block;
    let block_destruc = this.state.block_destruc;

    // console.log(power);

    // 先推入炸彈本身index
    let arr = [];
    arr.push(index);

    // 判斷是否為障礙物
    const isBlock = (x) => { return block.indexOf(x)!=-1; }

    // 判斷是否為可破壞障礙物
    const isBlockDestruc =  (x) => { return block_destruc.indexOf(x)!=-1; }

    // 定義方向
    const directions = ["top", "right", "bottom", "left"];

    // 判斷是否為邊界
    // @ nowIndex : 目前走到的位置
    const getIsBorder = 
    {
      "top"    : (nowIndex) => { return nowIndex - this.col < 0 ; },
      "right"  : (nowIndex) => { return (nowIndex + 1) % this.col == 0 ; },
      "bottom" : (nowIndex) => { return nowIndex + this.col >= this.col * this.row ; },
      "left"   : (nowIndex) => { return nowIndex % this.col == 0 ; }
    }

    // 取得下一個index
    // @ i：下一個次序
    // @ index：起始位置
    const getNextIndex = 
    {
      "top"    : (i, index) => { return index - this.col*i ; },
      "right"  : (i, index) => { return index + 1*i ; },
      "bottom" : (i, index) => { return index + this.col*i ; },
      "left"   : (i, index) => { return index - 1*i ; }
    }

    // 注意：
    // 如遇到可破壞之障礙物，爆炸範圍只能+1
    // 如遇到不可破壞之障礙物，爆炸範圍不可繼續增加

    // 先定義現在的上下左右位置，由於一開始還沒向外展開，故默認為中心點
    let now_left, now_right, now_top, now_bottom;
    now_left = now_right = now_top = now_bottom = index;

    // 判斷是否已經破壞已經障礙物了
    let isDestroyBlock_left, isDestroyBlock_right, isDestroyBlock_top, isDestroyBlock_bottom;
    isDestroyBlock_left = isDestroyBlock_right = isDestroyBlock_top = isDestroyBlock_bottom = false;

    for(let i=0 ; i<directions.length ; i++)
    {
        // 取得現在的方向
        let direction = directions[i];

        // 定義現在的上下左右位置，由於一開始還沒向外展開，故先默認為中心點
        let now_index = index;

        // 判斷是否已經破壞障礙物了
        let isDestroyBlock = false;

        for(let j=0; j<power ; j++)
        {
            // 先檢查目前是否在邊界，如果現在位置是null，代表已超出邊界
            let isBorder = now_index? getIsBorder[direction](now_index) : true;
            
            // console.log(direction, isBorder);

            // 下一個位置
            let nextIndex = isBorder || isDestroyBlock? null : getNextIndex[direction](j+1, index);
            
            // 判斷是否為不可破壞之障礙物
            if(isBlock(nextIndex)) nextIndex = null;
            
            // 判斷是否為可破壞障礙物
            if(isBlockDestruc(nextIndex)) isDestroyBlock = true;
            
            // console.log(nextIndex);

            // 刷新現在的上下左右位置，如果已經在邊界，則設為null
            now_index = isBorder? null : nextIndex;
            
            // 如果目前位置沒有被定義成null，則推入
            nextIndex!=null && arr.push(nextIndex);
        }
    }

    return arr;
  }


  // 判斷兩個arr是否相同
  isArraysEqual(a, b) 
  {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;
    for (let i = 0; i < a.length; i++) 
    {
      if (a[i] !== b[i]) return false;
    }
    return true;
  }


  render()
  {
    return (
      <div className="bomb-man-container">
        {this.drawStateBoard()}
        <div className="board">
          {this.drawCharacter(1)}
          {this.drawCharacter(2)}
          {this.drawBlocks()}
          {this.drawSupplys()}
        </div>
      </div>
    );
  }
  

}

export default BoardMain;

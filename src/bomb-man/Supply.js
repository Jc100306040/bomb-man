// 補給品

import React, { Component } from 'react';

class Supply extends Component 
{
  constructor(props)
  {
    super(props);
  }

  render()
  {
    return (
      <div className="supply" style={{backgroundColor : this.props.color, top : this.props.top, left : this.props.left}}>
      {this.props.text}
      </div>    
    );
  }
}

export default Supply;

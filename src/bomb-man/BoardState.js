// 角色狀態面板

import React, { Component } from 'react';
import {ability_type} from './const/const.js';

class BoardState extends Component 
{
  
  constructor(props)
  {
      super(props);

      this.player_1_data = this.props.player_1_data;
      this.player_2_data = this.props.player_2_data;
  }


  drawBalls(playerID, type)
  {
      let balls = [];
      let data = playerID==1? this.props.player_1_data : this.props.player_2_data;
      let num = null;
      let color = null;

      switch(type)
      {
          // 生命
          case ability_type.LIFE :
            color = "red";
            num = data.life || 0;
            break;

          // 爆破範圍
          case ability_type.BLAST_RANGE :
            color = "#FFBB00";
            num = data.blast_range_level || 0;
            break;

          // 炸彈數量 
          case ability_type.BOMB_NUM :
            color = "black";
            num = data.bomb_num_level || 0;
            break;
      }

      for(let i=0; i<num ;i++)
      {
          balls.push(
            <div className="level-ball" style={{background:color}}/>
            );
      }

      return balls;
  }


  render()
  {
    return (
      <div className="state-board">
          <div className="state-section">
              <div className="state-section-title" style={{float:'left'}} >PLAYER 1</div>
              <div className="state-section-item" style={{float:'left', borderLeft:'none'}} >
                  <div className="state-section-item-name state-section-item-name-left" style={{top:0}}>LIFE</div>
                  <div className="state-section-item-name state-section-item-name-left" style={{top:20}}>BOMB</div>
                  <div className="state-section-item-name state-section-item-name-left" style={{top:41}}>POWER</div>

                  <div className="state-section-item-level" style={{top:0, left:67}}>
                      {this.drawBalls(1, ability_type.LIFE)}
                  </div>
                  <div className="state-section-item-level" style={{top:20, left:67}}>
                      {this.drawBalls(1, ability_type.BOMB_NUM)}                  
                  </div>
                  <div className="state-section-item-level" style={{top:41, left:67}}>
                      {this.drawBalls(1, ability_type.BLAST_RANGE)}
                  </div>
                  
              </div>
          </div>
          
          <div className="state-section" style={{right:0}}>
              
              <div className="state-section-item" style={{float:'right', borderLeft:'none'}} >

                  <div className="state-section-item-name state-section-item-name-left" style={{top:0}}>LIFE</div>
                  <div className="state-section-item-name state-section-item-name-left" style={{top:20}}>BOMB</div>
                  <div className="state-section-item-name state-section-item-name-left" style={{top:41}}>POWER</div>

                  <div className="state-section-item-level" style={{top:0, left:67}}>
                      {this.drawBalls(2, ability_type.LIFE)}
                  </div>
                  <div className="state-section-item-level" style={{top:20, left:67}}>
                      {this.drawBalls(2, ability_type.BOMB_NUM)} 
                  </div>
                  <div className="state-section-item-level" style={{top:41, left:67}}>
                      {this.drawBalls(2, ability_type.BLAST_RANGE)}
                  </div>
              </div>

              <div className="state-section-title" style={{float:'right'}} >PLAYER 2</div>
          </div> 
      </div>    
    );
  }
}

export default BoardState;

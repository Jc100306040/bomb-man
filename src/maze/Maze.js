// 遊戲介面：迷宮

import React, { Component } from 'react';
import Grid from '../common/Grid.js';

class Maze extends Component 
{

    constructor(props)
    {
        super(props);

        // 定義基本變數
        this.gridWidth = this.gridHeight = 100;
        this.boardWidth = 800;
        this.boardHeight = 600;
        this.row = this.boardHeight / this.gridHeight;
        this.col = this.boardWidth / this.gridWidth;

        // 測試用變數
        this.showText = true;
    }


    // 畫出網格
    drawGrids()
    {
        let grids = [];
        let number = this.col * this.row;

        let entranceAndExitArr = this.getEntranceAndExit(number);
        console.log(entranceAndExitArr);

        for(let i=1 ; i<=number ; i++)
        {
            let color = entranceAndExitArr.indexOf(i)==-1 ? "#8E8E8E" : "#844200"; 
            
            grids.push(
              <Grid color={color} text={i} showText={this.showText}
                width={this.gridWidth} height={this.gridHeight}/>
              );
        }

        return grids;
    }


    // 隨機取得出入口
    getEntranceAndExit(number)
    {
        // 邊緣格暫存Arr
        let borderPositionArr = [];

        for(let i=1 ; i<=number ; i++)
        {
            // 出入口只會出現在邊緣格
            let isUpBorder      = i <= this.col;
            let isBottomBorder  = (this.row - 1)*this.col <= i;
            let isLeftBorder    = (i-1)%this.col == 0;
            let isRightBorder   = i%this.col == 0;

            (isUpBorder || isBottomBorder || 
              isRightBorder || isLeftBorder) && borderPositionArr.push(i);
        }

        // 隨機取得兩個作為出入口
        let result = [];

        while(result.length < 2)
        {
            let randomIndex = Math.floor(Math.random() * borderPositionArr.length);
            let randomPosition = borderPositionArr[randomIndex];
            
            // 如果已經找到一個了，就做檢查
            if(result.length)
            {
                // 不存在，且不相鄰，才可塞入
                let firstPosition = result[0];
                let isClose = (randomPosition==firstPosition+1) || (randomPosition==firstPosition+this.col) ||
                  (randomPosition==firstPosition-1) || (randomPosition==firstPosition-this.col);

                let isTheSame = randomPosition==firstPosition;

                if(!isTheSame && !isClose) result.push(randomPosition);
            }
            else
            {
                result.push(randomPosition);
            }
        }

        return result;
    }


    render()
    {
        return (
          <div className="maze-container">
             <div className="board-maze">
              {this.drawGrids()}
             </div>
          </div>
        );
    }

}

export default Maze;

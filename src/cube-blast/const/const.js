// 定義會出現的顏色設置(黃、紅、藍、綠、紫、灰)

const hoverAlpha = 1;

const color = 
    {
        YELLOW    : 0,
        RED       : 1,
        BLUE      : 2,
        GREEN     : 3,
        PURPLE    : 4,
        GREY      : 5,
        ORANGE    : 6,
        BROWN     : 7,
        D_GREEN   : 8,
        D_BLUE    : 9,
        CAKE      : 10,
        SALMON    : 11,
        VIOLET    : 12,
        L_BLUE    : 13,
        TOTAL_NUM : 14
    };

const color_data = 
    {
        [color.YELLOW] :
            {
                normal : 'rgba(255, 255, 0, 0.8)',
                hover  : 'rgba(255, 255, 0,' + hoverAlpha +')',
            },
        [color.RED] :
            {
                normal : 'rgba(255, 0, 0, 0.8)',
                hover  : 'rgba(255, 0, 0,' + hoverAlpha +')',
            },
        [color.BLUE] :
            {
                normal : 'rgba(51, 255, 255, 0.8)',
                hover  : 'rgba(51, 255, 255,' + hoverAlpha +')',
            },
        [color.GREEN] :
            {
                normal : 'rgba(102, 221, 0, 0.8)',
                hover  : 'rgba(102, 221, 0,' + hoverAlpha +')',
            },
        [color.PURPLE] :
            {
                normal : 'rgba(240, 187, 255, 0.8)',
                hover  : 'rgba(240, 187, 255,' + hoverAlpha +')',
            },
        [color.GREY] :
            {
                normal : 'rgba(170, 170, 170, 0.8)',
                hover  : 'rgba(170, 170, 170,' + hoverAlpha +')',
            },
        [color.ORANGE] :
            {
                normal : 'rgba(255, 187, 0, 0.8)',
                hover  : 'rgba(255, 187, 0,' + hoverAlpha +')'
            },
        [color.BROWN] :
            {
                normal : 'rgba(136, 102, 0, 0.8)',
                hover  : 'rgba(136, 102, 0,' + hoverAlpha +')'
            },
        [color.D_GREEN] :
            {
                normal : 'rgba(0, 100, 0, 0.8)',
                hover  : 'rgba(0, 100, 0,' + hoverAlpha +')'
            },
        [color.D_BLUE] :
            {
                normal : 'rgba(0, 0, 139, 0.8)',
                hover  : 'rgba(0, 0, 139,' + hoverAlpha +')'
            },
        [color.CAKE] :
            {
                normal : 'rgba(255, 250, 205, 0.8)',
                hover  : 'rgba(255 ,250, 205,' + hoverAlpha +')'
            },
        [color.SALMON] :
            {
                normal : 'rgba(250, 128, 114, 0.8)',
                hover  : 'rgba(250, 128, 114,' + hoverAlpha +')'
            },
        [color.VIOLET] :
            {
                normal : 'rgba(138, 43, 226, 0.8)',
                hover  : 'rgba(138, 43, 226,' + hoverAlpha +')'
            },
         [color.L_BLUE] :
            {
                normal : 'rgba(224, 255, 255, 0.8)',
                hover  : 'rgba(224, 255, 255,' + hoverAlpha +')'
            },
    };




export {color_data, color};
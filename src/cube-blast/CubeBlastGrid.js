// 格子元件：消除方塊遊戲專用

import React, { Component } from 'react';
import Grid from '../common/Grid.js';

class GridBlastGrid extends Grid 
{

    // @override
    constructor(props)
    {
        super(props);

        // 滑鼠事件
        this.onClickFunc = this.props.onClickFunc;
        this.onMouseOverFunc = this.props.onMouseOverFunc;
        this.onMouseOutFunc = this.props.onMouseOutFunc;
    }


    // @override
    getOnClickFunc()
    { 
        return ()=> { if(this.props.onClickFunc) this.onClickFunc(this.index); }
    }


    // @override
    getOnMouseOverFunc()
    { 
        return ()=> { if(this.props.onMouseOverFunc) this.onMouseOverFunc(this.index); }
    }


    // @override
    getOnMouseOutFunc()
    { 
        return ()=> { if(this.props.onMouseOutFunc) this.onMouseOutFunc(this.index); }
    }


    // @override
    getAdditionalStyle()
    {
        let additionalStyle = {};
        let borderColor = this.props.isHover? 'rgba(0, 0, 0, 1)' : 'rgba(0, 0, 0, 1)';
        let borderStyle = this.props.isHover? 'solid 2.5px ' + borderColor : 'solid 0.5px ' + borderColor;

        additionalStyle = 
            {
                opacity     : this.props.opacity || 1,
                cursor      : this.props.isCursor ? 'pointer' : null,
                boxSizing   : 'border-box',
                border      : borderStyle
            }
        
        return additionalStyle;
    }

}

export default GridBlastGrid;

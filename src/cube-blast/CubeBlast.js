// 遊戲介面：消除方塊

import React, { Component } from 'react';
import { color, color_data } from './const/const.js';
import CubeBlastGrid from './CubeBlastGrid.js';
import StateBoard from './StateBoard.js';

class CubeBlast extends Component 
{

    constructor(props)
    {
        super(props);

        // 定義基本變數
        this.gridWidth = this.gridHeight = 50;
        this.boardWidth = 900;
        this.boardHeight = 600;
        this.row = this.boardHeight / this.gridHeight;
        this.col = this.boardWidth / this.gridWidth;
        this.totalNum = this.row * this.col;
        
        // 定義分數乘加的方塊數量區間
        this.scoreCalcLevel = 10;

        // 定義每個方塊的分數
        this.singleCubeScore = 5;

        // 定義升等的分數區間條件
        this.scoreToLevelUp = 1000;

        // 紀錄分數
        this.score = 0;

        // 紀錄等級
        this.level = 1;

        // 紀錄最低顏色數量
        this.colorNum = 3;

        this.state = 
        {
            // 方塊資訊
            blockDataObj  : this.getRandomBlocksData(),
        }

        // 紀錄所有配對(二維矩陣)
        this.answer = []; 

        // 紀錄所有行的 index 資訊(二維矩陣)
        // 此二維矩陣是為了計算消除方塊，下落後的位置、顏色所使用
        this.colIndexArr = this.getColIndexArr(this.col, this.row);

        // 記錄所有結果
        this.answer = this.getAllAnswer(this.state.blockDataObj);

        // 測試用變數
        this.showText = false;
    }


    // 畫出網格
    drawGrids()
    {
        let grids = [];
        let blockDataObj = this.state.blockDataObj;
       
        for(let i=0 ; i<this.totalNum ; i++)
        {
            let colorType = blockDataObj[i].colorType; 
            let isHover = blockDataObj[i].isHover; 
            let color = colorType==null ? null : isHover ? 
                color_data[colorType].hover : color_data[colorType].normal;
            let isCursor = true;
            
            grids.push(
              <CubeBlastGrid color={color} text={i} showText={this.showText} index={i} isHover={isHover} isCursor={isCursor}
                width={this.gridWidth} height={this.gridHeight}
                onClickFunc={this.onCubeClick.bind(this)} onMouseOverFunc={this.onCubeHoverIn.bind(this)} 
                onMouseOutFunc={this.onCubeHoverOut.bind(this)} />
              );
        }

        return grids;
    }


    // 方塊：點擊事件
    onCubeClick(index)
    {
        let answer = this.answer;
        let singleAnswerArr = this.getSingleAnswerByIndex(index, answer);
        if(!singleAnswerArr || !singleAnswerArr.length) return;
        this.clearCube(singleAnswerArr);
        this.calcScore(singleAnswerArr.length);
    }


    // 方塊：滑入
    onCubeHoverIn(index)
    {
        let answer = this.answer;
        // console.log(answer);
        let singleAnswerArr = this.getSingleAnswerByIndex(index, answer);
        if(!singleAnswerArr || !singleAnswerArr.length) return;
        this.changeColorByHover(singleAnswerArr, true);
    }


    // 方塊：滑出
    onCubeHoverOut(index)
    {
        let answer = this.answer;
        let singleAnswerArr = this.getSingleAnswerByIndex(index, answer);
        if(!singleAnswerArr || !singleAnswerArr.length) return;
        this.changeColorByHover(singleAnswerArr, false);
    }


    // 改變單組配對的顏色
    changeColorByHover(singleAnswerArr, isHover)
    {   
        let blockDataObj = Object.assign({}, this.state.blockDataObj);
        for(let i=0 ; i<singleAnswerArr.length ; i++)
        {
            let _index = singleAnswerArr[i];
            blockDataObj[_index].color = color;
            blockDataObj[_index].isHover = isHover;
        }
        this.setState({blockDataObj:blockDataObj});
    }


    // 消除方塊
    clearCube(singleAnswerArr)
    {
        let blockDataObj = Object.assign({}, this.state.blockDataObj);

        // 先將被消除方塊顏色設為 null
        for(let i=0 ; i<singleAnswerArr.length ; i++)
        {
            let _index = singleAnswerArr[i];
            blockDataObj[_index].colorType = null;
        }

        // 設置方塊落下邏輯
        let colColorArr = [];
        for(let i=0 ; i<this.colIndexArr.length ; i++)
        {
            // 先將落下的方塊用矩陣方式依「行」排列 -> ex: [null, null, null, 黃, 藍, 紅...] 
            let singleColIndexArr = this.colIndexArr[i];
            let singleColColorArr = singleColIndexArr.map( x => this.getColorType(x, blockDataObj)).filter(x => x!=null);
            singleColColorArr = this.arrAfterUnshiftColor(this.row, singleColColorArr);
            
            // console.log(singleColColorArr);

            // 依照 index 將顏色置入 blockDataObj 中
            for(let j=0 ; j<singleColColorArr.length ; j++)
            {
                let realIndex = i + j*this.col;
                blockDataObj[realIndex].colorType = singleColColorArr[j];
                blockDataObj[realIndex].isHover = false;
            }
        }

        // 重新計算配對組合
        this.answer = this.getAllAnswer(blockDataObj);
        
        this.setState({blockDataObj:blockDataObj});
    }


    // 刷新遊戲
    refreshGame()
    {
        // 刷新方塊
        let newBlockDataObj = this.getRandomBlocksData();

        // 重新計算配對組合
        this.answer = this.getAllAnswer(newBlockDataObj);

        this.setState({blockDataObj:newBlockDataObj});
    }


    // 隨機取得資料
    getRandomBlocksData()
    {
        let obj = {};
        for(let i=0 ; i<this.totalNum; i++)
        {
            obj[i] = {};
            obj[i].colorType = this.getRandomColorType();
            obj[i].isHover = false;
        }
        return obj;
    }


    // 計算所有配對
    // 方法：從左至右、上至下依次將方塊丟入各自矩陣，每輪到一個方塊，需檢查上方與左方是否同色
    getAllAnswer(blockData)
    {
        // 定義一個二維 array 
        let resultArr = [];

        // 判斷矩陣是否相同
        const isArrayEqual = (arr_1, arr_2) =>
        {
            if (arr_1 === arr_2) return true;
            if (arr_1 == null || arr_2 == null) return false;
            if (arr_1.length != arr_2.length) return false;
            for (let i = 0; i < arr_1.length; i++) 
            {
                if (arr_1[i] !== arr_2[i]) return false;
            }
            return true;
        }

        // 移除矩陣
        const deleteArr = (deleteArr) => {
            resultArr = resultArr.filter( x => !isArrayEqual(x, deleteArr));
        }

        // 判斷顏色是否相同
        const isColorTheSame = (index_1, index_2) => {
            return this.getColorType(index_1, blockData) == this.getColorType(index_2, blockData);
        }

        const isColorTheSame_left = (index) => {
            let isBorderleft = (index+1) % this.col == 1;
            return isBorderleft? false : isColorTheSame(index, index-1);
        };

        const isColorTheSame_top = (index) => {
            let isBorderTop = (index+1) - this.col <= 0;
            return isBorderTop? false : isColorTheSame(index, index - this.col);
        };

        // 組合
        const connectTop = (index) => {
            let top_index = index - this.col;
            let arr = this.getSingleAnswerByIndex(top_index, resultArr);
            if(arr) arr.push(index);
        }

        const connectLeft = (index) => {
            let left_index = index - 1;
            let arr = this.getSingleAnswerByIndex(left_index, resultArr);
            if(arr) arr.push(index);
        }

        const connectLeftAndTop = (index) => {
            let left_index = index - 1;
            let arr_l = this.getSingleAnswerByIndex(left_index, resultArr);
            let top_index = index - this.col;
            let arr_t = this.getSingleAnswerByIndex(top_index, resultArr);
            if(!arr_l || !arr_t) return;
            if(isArrayEqual(arr_l, arr_t))
            {
                arr_l.push(index);
            }
            else
            {
                let newConnection = arr_l.concat([index]).concat(arr_t);
                deleteArr(arr_t);
                deleteArr(arr_l);
                resultArr.push(newConnection);
            }
        }

        for(let i=0 ; i<this.totalNum ; i++)
        {
            // 取得此方塊 index
            let index = i;

            // 先定義一個暫存 array
            let connectionArr;

            // console.log(index, isColorTheSame_left(index), isColorTheSame_top(index));

            // 判斷位置與顏色，並進行組合
            if(!isColorTheSame_left(index) && !isColorTheSame_top(index))
            {
                connectionArr = [index];
                resultArr.push(connectionArr);
            }
            else if(isColorTheSame_left(index) && isColorTheSame_top(index))
            {
                connectLeftAndTop(index);
            }
            else if(isColorTheSame_left(index))
            {
                connectLeft(index);
            }
            else if(isColorTheSame_top(index))
            {
                connectTop(index);
            }
        }
        
        // return resultArr.slice();
        return resultArr.filter( x => x.length>1);
    }


    // 設置行 index 資訊
    getColIndexArr(col, row)
    {
        let motherArr = [];
        for(let i=0 ; i<col ; i++)
        {
            let arr = [];
            for(let j=0 ; j<row ; j++)
            {
                let index = i + j*col;
                arr.push(index);
            }
            motherArr.push(arr);
        }
        // console.log(motherArr);
        return motherArr;
    }


    // 根據特定 index 取得 color
    getColorType(index, blockData)
    {
        return blockData[index]? blockData[index].colorType : null;
    }


    // 根據特定 index 從解答組合二維陣列中取得單一 answer arr
    getSingleAnswerByIndex = (index, dataArr) => {
        let arr = [];
        dataArr.forEach(x => { 
            if(x.indexOf(index)!=-1) arr = x; 
        });
        return !arr.length? null : arr;
    }


    // 在 arr 的前面塞入 「隨機色碼」 值至固定數量
    arrAfterUnshiftColor = (number, arr) => { 
        if(typeof arr != "object" || !Array.isArray(arr)) return arr;
        if(arr.length >= number) return arr;
        let _arr = arr.slice();
        let num = number - arr.length;
        for(let i=0 ; i<num ; i++) _arr.unshift(this.getRandomColorType());
        return _arr;
    }


    // 取得隨機顏色類別
    getRandomColorType()
    {
        let randomColorType = Math.floor( Math.random() * this.colorNum );
        return randomColorType;
    }


    // 計算分數
    calcScore(cubeNum)
    {    
        let level = Math.floor(cubeNum/this.scoreCalcLevel);
        let multiplier =  Math.pow(2, level);
        let addScore = cubeNum * this.singleCubeScore * multiplier;
        this.score += addScore;
        
        // 計算等級
        this.calcLevel(this.score); 
    }


    // 計算等級
    calcLevel(totalScore)
    {
        let newLevel = Math.floor(totalScore / this.scoreToLevelUp) + 1;
        if(newLevel > this.level)
        {
            // 增加顏色數量
            this.colorNum += (newLevel-this.level);
            if(this.colorNum > color.TOTAL_NUM)  this.colorNum = color.TOTAL_NUM;

            // 等級存取
            this.level = newLevel;
            
            // 刷新遊戲
            this.refreshGame();
        }
    }


    render()
    {
        return (
          <div className="maze-container">
             <div className="board-maze">
                {this.drawGrids()}
             </div>
             <StateBoard level={this.level} score={this.score}/>
          </div>
        );
    }

}

export default CubeBlast;

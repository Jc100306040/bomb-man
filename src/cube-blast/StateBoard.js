// 關卡控制、計分板：方塊消除

import React, { Component } from 'react';

class StateBoard extends Component 
{


    constructor(props)
    {
        super(props);
    }


    render()
    {
        // 外層
        let style_container = 
        {
            position : 'absolute',
            top : '-2px',
            right : 0,
        };

        // 內層項目
        let style_item = 
        {
            border : 'solid 3px black',
            width : '200px',
            height : '104px',
            boxSizing : 'border-box',
            marginBottom : '46px',
        };

        // 再內層項目
        // 基本設置
        let style_sub_item_basic = 
        {
            width : '194px',
            height : '49px',
            textAlign : 'center',
            lineHeight : '49px',
            fontWeight : 'bold'
        };

        let style_sub_item_title = Object.assign({}, style_sub_item_basic, {
            fontSize : '22px',
            letterSpacing : '3px',
            background : 'rgba(0, 0, 0, 0.15)'
        });

        let style_sub_item_value = Object.assign({}, style_sub_item_basic, {
            fontSize : '30px',
            letterSpacing : '3px',
        });
        
        return (
          <div style={style_container}>
             <div style={style_item}>
                <div className="" style={style_sub_item_title}>LEVEL</div>
                <div className="" style={style_sub_item_value}>{this.props.level}</div>
             </div>
             <div style={style_item}>
                <div className="" style={style_sub_item_title}>SCORE</div>
                <div className="" style={style_sub_item_value}>{this.props.score}</div>
             </div>
          </div>
        );
    }

}

export default StateBoard;
